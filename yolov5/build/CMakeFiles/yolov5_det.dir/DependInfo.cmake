# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CUDA"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CUDA
  "/yolo/src/preprocess.cu" "/yolo/build/CMakeFiles/yolov5_det.dir/src/preprocess.cu.o"
  )
set(CMAKE_CUDA_COMPILER_ID "NVIDIA")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CUDA
  "API_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CUDA_TARGET_INCLUDE_PATH
  "../src"
  "../plugin"
  "/usr/include/opencv4"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/yolo/src/calibrator.cpp" "/yolo/build/CMakeFiles/yolov5_det.dir/src/calibrator.cpp.o"
  "/yolo/src/model.cpp" "/yolo/build/CMakeFiles/yolov5_det.dir/src/model.cpp.o"
  "/yolo/src/postprocess.cpp" "/yolo/build/CMakeFiles/yolov5_det.dir/src/postprocess.cpp.o"
  "/yolo/yolov5_det.cpp" "/yolo/build/CMakeFiles/yolov5_det.dir/yolov5_det.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "API_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/cuda/include"
  "../src"
  "../plugin"
  "/usr/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/yolo/build/CMakeFiles/myplugins.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
