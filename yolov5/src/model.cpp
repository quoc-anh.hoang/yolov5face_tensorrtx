#include "model.h"
#include "calibrator.h"
#include "config.h"
#include "yololayer.h"

#include <iostream>
#include <fstream>
#include <map>
#include <cassert>
#include <cmath>
#include <cstring>

using namespace nvinfer1;

void print_dims(ITensor& input) {
  Dims32 inputDims = input.getDimensions();
  for (int i = 0; i < inputDims.MAX_DIMS; i++) {
    std::cout << inputDims.d[i] << " ";
  }
  std::cout << std::endl;
}

static std::map<std::string, Weights> loadWeights(const std::string file) {
  std::cout << "Loading weights: " << file << std::endl;
  std::map<std::string, Weights> weightMap;

  // Open weights file
  std::ifstream input(file);
  assert(input.is_open() && "Unable to load weight file. please check if the .wts file path is right!!!!!!");

  // Read number of weight blobs
  int32_t count;
  input >> count;
  assert(count > 0 && "Invalid weight map file.");
  
  while (count--) {
    Weights wt{ DataType::kFLOAT, nullptr, 0 };
    uint32_t size;
    
    // Read name and type of blob
    std::string name;
    input >> name >> std::dec >> size;
    wt.type = DataType::kFLOAT;
    
    // Load blob
    uint32_t* val = reinterpret_cast<uint32_t*>(malloc(sizeof(val) * size));
    for (uint32_t x = 0, y = size; x < y; ++x) {
      input >> std::hex >> val[x];
    }
    wt.values = val;
    
    wt.count = size;
    weightMap[name] = wt;
  }
  
  return weightMap;
}

static int get_width(int x, float gw, int divisor = 8) {
  return int(ceil((x * gw) / divisor)) * divisor;
}

static int get_depth(int x, float gd) {
  if (x == 1) return 1;
  int r = round(x * gd);
  if (x * gd - int(x * gd) == 0.5 && (int(x * gd) % 2) == 0) {
    --r;
  }
  return std::max<int>(r, 1);
}

static std::vector<std::vector<float>> getAnchors(std::map<std::string, Weights>& weightMap, std::string lname) {
    std::vector<std::vector<float>> anchors;
    Weights wts = weightMap[lname + ".anchor_grid"];
    int anchor_len = kNumAnchor * 2;
    for (int i = 0; i < wts.count / anchor_len; i++) {
        auto *p = (const float*)wts.values + i * anchor_len;
        std::vector<float> anchor(p, p + anchor_len);
        anchors.push_back(anchor);
    }
    return anchors;
}

static IScaleLayer* addBatchNorm2d(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, std::string lname, float eps) {
  float* gamma = (float*)weightMap[lname + ".weight"].values;
  float* beta = (float*)weightMap[lname + ".bias"].values;
  float* mean = (float*)weightMap[lname + ".running_mean"].values;
  float* var = (float*)weightMap[lname + ".running_var"].values;
  int len = weightMap[lname + ".running_var"].count;

  float* scval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
  for (int i = 0; i < len; i++) {
    scval[i] = gamma[i] / sqrt(var[i] + eps);
  }
  Weights scale{ DataType::kFLOAT, scval, len };

  float* shval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
  for (int i = 0; i < len; i++) {
    shval[i] = beta[i] - mean[i] * gamma[i] / sqrt(var[i] + eps);
  }
  Weights shift{ DataType::kFLOAT, shval, len };
  
  float* pval = reinterpret_cast<float*>(malloc(sizeof(float) * len));
  for (int i = 0; i < len; i++) {
    pval[i] = 1.0;
  }
  Weights power{ DataType::kFLOAT, pval, len };

  weightMap[lname + ".scale"] = scale;
  weightMap[lname + ".shift"] = shift;
  weightMap[lname + ".power"] = power;
  IScaleLayer* scale_1 = network->addScale(input, ScaleMode::kCHANNEL, shift, scale, power);
  assert(scale_1);
  return scale_1;
}

static ILayer* convBlock(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, int outch, int ksize, int s, int g, int p, std::string lname) {
  Weights emptywts{ DataType::kFLOAT, nullptr, 0 };
  IConvolutionLayer* conv1 = network->addConvolutionNd(input, outch, DimsHW{ ksize, ksize }, weightMap[lname + ".conv.weight"], emptywts);
  assert(conv1);
  conv1->setStrideNd(DimsHW{ s, s });
  conv1->setPaddingNd(DimsHW{ p, p });
  conv1->setNbGroups(g);
  conv1->setName((lname + ".conv").c_str());
  IScaleLayer* bn1 = addBatchNorm2d(network, weightMap, *conv1->getOutput(0), lname + ".bn", 1e-3);

  // silu = x * sigmoid
  auto sig = network->addActivation(*bn1->getOutput(0), ActivationType::kSIGMOID);
  assert(sig);
  auto ew = network->addElementWise(*bn1->getOutput(0), *sig->getOutput(0), ElementWiseOperation::kPROD);
  assert(ew);
  return ew;
}

void print_values(ITensor& input) {
  Dims inputDims = input.getDimensions();
  float_t* tensorData;
  tensorData = reinterpret_cast<float_t*>(&input);
  for (int i = 0; i < inputDims.d[0] ; i++) {
    std::cout << tensorData[i] << " ";
  }
  std::cout << std::endl;
}

static ILayer* StemBlock(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input,  std::string lname) {
  
  auto stem_1 = convBlock(network, weightMap, input, 32, 3, 2, 1, 1, lname + ".stem_1");
  auto stem_2a = convBlock(network, weightMap, *stem_1->getOutput(0), 16, 1, 1, 1, 0, lname + ".stem_2a");
  auto stem_2b = convBlock(network, weightMap, *stem_2a->getOutput(0), 32, 3, 2, 1, 1, lname + ".stem_2b");

  auto stem_2p = network->addPoolingNd(*stem_1->getOutput(0), PoolingType::kMAX, DimsHW{2, 2});
  stem_2p->setStride({2, 2});
  stem_2p->setPadding({0, 0});

  ITensor* y2 = stem_2b->getOutput(0);
  ITensor* inputTensors[] = {y2, stem_2p->getOutput(0)};
  
  auto cat = network->addConcatenation(inputTensors, 2);
  auto stem_3 = convBlock(network, weightMap, *cat->getOutput(0), 32, 1, 1, 1, 0, lname + ".stem_3");

  return stem_3;
}

static ILayer* ShuffleV2BlockBranch1(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, int inch, int outch, int s, std::string lname) {
  int branch_features = outch / 2;
  
  Weights emptywts1{ DataType::kFLOAT, nullptr, 0 };
  IConvolutionLayer* conv1 = network->addConvolutionNd(input, inch, DimsHW{ 3, 3 }, weightMap[lname + ".0.weight"], emptywts1);
  assert(conv1);
  conv1->setStrideNd(DimsHW{ s, s });
  conv1->setPaddingNd(DimsHW{ 1, 1 });
  conv1->setNbGroups(inch);
  conv1->setName((lname + ".0.conv1_branch1").c_str());

  IScaleLayer* bn1 = addBatchNorm2d(network, weightMap, *conv1->getOutput(0), lname + ".1", 1e-3);
  
  Weights emptywts2{ DataType::kFLOAT, nullptr, 0 };
  IConvolutionLayer* conv2 = network->addConvolutionNd(*bn1->getOutput(0), branch_features, DimsHW{ 1, 1 }, weightMap[lname + ".2.weight"], emptywts2);
  assert(conv2);
  conv2->setStrideNd(DimsHW{ 1, 1 });
  conv2->setPaddingNd(DimsHW{ 0, 0 });
  conv2->setNbGroups(1);
  conv2->setName((lname + ".conv2_branch1").c_str());
  
  IScaleLayer* bn2 = addBatchNorm2d(network, weightMap, *conv2->getOutput(0), lname + ".3", 1e-3);

  // silu = x * sigmoid
  auto sig = network->addActivation(*bn2->getOutput(0), ActivationType::kSIGMOID);
  assert(sig);
  auto branch1 = network->addElementWise(*bn2->getOutput(0), *sig->getOutput(0), ElementWiseOperation::kPROD);
  assert(branch1);
  return branch1;
}

static ILayer* ShuffleV2BlockBranch2(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, int inch, int outch, int s, std::string lname) {
  int branch_features = outch / 2;
  
  unsigned int* a = reinterpret_cast<unsigned int*>(const_cast<void*>(weightMap[lname + ".0.weight"].values));
  Weights emptywts1{ DataType::kFLOAT, nullptr, 0 };
  IConvolutionLayer* conv1 = network->addConvolutionNd(input, branch_features, DimsHW{ 1, 1 }, weightMap[lname + ".0.weight"], emptywts1);
  assert(conv1);
  conv1->setStrideNd(DimsHW{ 1, 1 });
  conv1->setPaddingNd(DimsHW{ 0, 0 });
  conv1->setNbGroups(1);  
  conv1->setName((lname + ".conv1_branch2").c_str());

  IScaleLayer* bn1 = addBatchNorm2d(network, weightMap, *conv1->getOutput(0), lname + ".1", 1e-3);
  auto sig1 = network->addActivation(*bn1->getOutput(0), ActivationType::kSIGMOID);
  assert(sig1);
  auto silu1 = network->addElementWise(*bn1->getOutput(0), *sig1->getOutput(0), ElementWiseOperation::kPROD);
  assert(silu1);
  //depthwise_conv
  Weights emptywts_depthwise{ DataType::kFLOAT, nullptr, 0 };
  IConvolutionLayer* depthwise_conv = network->addConvolutionNd(*silu1->getOutput(0), branch_features, DimsHW{ 3, 3 }, weightMap[lname + ".3.weight"], emptywts_depthwise);
  assert(depthwise_conv);
  depthwise_conv->setStrideNd(DimsHW{ s, s });
  depthwise_conv->setPaddingNd(DimsHW{ 1, 1 });
  depthwise_conv->setNbGroups(branch_features);
  depthwise_conv->setName((lname + ".depthwise_conv_branch2").c_str());
  IScaleLayer* bn_depthwise = addBatchNorm2d(network, weightMap, *depthwise_conv->getOutput(0), lname + ".4", 1e-3);
  
  Weights emptywts2{ DataType::kFLOAT, nullptr, 0 };
  IConvolutionLayer* conv2 = network->addConvolutionNd(*bn_depthwise->getOutput(0), branch_features, DimsHW{ 1, 1 }, weightMap[lname + ".5.weight"], emptywts2);
  assert(conv2);
  conv2->setStrideNd(DimsHW{ 1, 1 });
  conv2->setPaddingNd(DimsHW{0, 0});
  conv2->setNbGroups(1);
  conv2->setName((lname + ".conv2_branch2").c_str());
  IScaleLayer* bn2 = addBatchNorm2d(network, weightMap, *conv2->getOutput(0), lname + ".6", 1e-3);
  auto sig2 = network->addActivation(*bn2->getOutput(0), ActivationType::kSIGMOID);
  assert(sig2);
  auto silu2 = network->addElementWise(*bn2->getOutput(0), *sig2->getOutput(0), ElementWiseOperation::kPROD);
  assert(silu2);
  
  return silu2;
}

static ILayer* channel_shuffle(INetworkDefinition *network, ITensor& input, int groups, std::string lname) {
  Dims inputDims = input.getDimensions();
  int channelsPerGoup = inputDims.d[0] / groups;
  Dims32 reshapeDims;
  reshapeDims.d[0] = groups;
  reshapeDims.d[1] = channelsPerGoup;
  reshapeDims.d[2] = inputDims.d[1];
  reshapeDims.d[3] = inputDims.d[2];
  reshapeDims.nbDims = 4;
  IShuffleLayer* reshapeLayer = network->addShuffle(input);
  reshapeLayer->setReshapeDimensions(reshapeDims);
  reshapeLayer->setName((lname + ".reshape_shuffle_layer1").c_str());
  
  nvinfer1::Permutation perm;
  perm.order[0] = 1;
  perm.order[1] = 0;
  perm.order[2] = 2;
  perm.order[3] = 3;

  IShuffleLayer* transposeLayer = network->addShuffle(*reshapeLayer->getOutput(0));
  transposeLayer->setFirstTranspose(perm);
  transposeLayer->setName((lname + ".transpose_layer").c_str());

  IShuffleLayer* reshapeLayer2 = network->addShuffle(*transposeLayer->getOutput(0));
  reshapeLayer2->setReshapeDimensions(inputDims);
  reshapeLayer2->setName((lname + ".reshape_shuffle_layer2").c_str());
  
  return reshapeLayer2;
}

static ILayer* ShuffleV2Block(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, int inch, int outch, int s, std::string lname) {
  if (s > 1) {

    auto branch1 = ShuffleV2BlockBranch1(network, weightMap, input, inch, outch, s, lname + ".branch1");
    auto branch2 = ShuffleV2BlockBranch2(network, weightMap, input, inch, outch, s, lname + ".branch2");
    ITensor* inputTensors[] = {branch1->getOutput(0), branch2->getOutput(0)};
    auto cat = network->addConcatenation(inputTensors, 2);
    auto out_shuffle = channel_shuffle(network, *cat->getOutput(0), 2, lname); //chua xong

    return out_shuffle;
  }
  else {
    
    Dims32 dx1 = input.getDimensions();
    dx1.nbDims = 4;

    ISliceLayer* sliceLayer1 = network->addSlice(input, Dims3{0, 0, 0}, Dims3{dx1.d[0]/2, dx1.d[1], dx1.d[2]}, Dims3{ 1, 1, 1});

    sliceLayer1->setName((lname + ".split1_branch2").c_str());
    assert(sliceLayer1);
    ITensor* x1 = sliceLayer1->getOutput(0);

    ISliceLayer* sliceLayer2 = network->addSlice(input, Dims3{dx1.d[0]/2, 0, 0}, Dims3{dx1.d[0]/2, dx1.d[1], dx1.d[2]}, Dims3{1, 1, 1});

    sliceLayer2->setName((lname + ".split2_branch2").c_str());
    assert(sliceLayer2);

    auto branch2 = ShuffleV2BlockBranch2(network, weightMap, *sliceLayer2->getOutput(0), inch, outch, s, lname + ".branch2");

    ITensor* inputTensors[] = {x1, branch2->getOutput(0)};
    auto cat = network->addConcatenation(inputTensors, 2);

    auto out_shuffle = channel_shuffle(network, *cat->getOutput(0), 2, lname); 

    return out_shuffle;
  }
}

static ILayer* bottleneck(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, int c1, int c2, bool shortcut, int g, float e, std::string lname) {
  auto cv1 = convBlock(network, weightMap, input, (int)((float)c2 * e), 1, 1, 1, 0, lname + ".cv1");
  auto cv2 = convBlock(network, weightMap, *cv1->getOutput(0), c2, 3, 1, g, 1, lname + ".cv2");
  if (shortcut && c1 == c2) {
    auto ew = network->addElementWise(input, *cv2->getOutput(0), ElementWiseOperation::kSUM);
    return ew;
  }
  return cv2;
}

static ILayer* C3(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, ITensor& input, int c1, int c2, int n, bool shortcut, int g, float e, std::string lname) {
  int c_ = (int)((float)c2 * e);

  auto cv1 = convBlock(network, weightMap, input, c_, 1, 1, 1, 0, lname + ".cv1");
  auto cv2 = convBlock(network, weightMap, input, c_, 1, 1, 1, 0, lname + ".cv2");
  ITensor *y1 = cv1->getOutput(0);
  for (int i = 0; i < n; i++) {
    auto b = bottleneck(network, weightMap, *y1, c_, c_, shortcut, g, 1.0, lname + ".m." + std::to_string(i));
    y1 = b->getOutput(0);
  }
  
  ITensor* inputTensors[] = { y1, cv2->getOutput(0) };
  auto cat = network->addConcatenation(inputTensors, 2);
  auto cv3 = convBlock(network, weightMap, *cat->getOutput(0), c2, 1, 1, 1, 0, lname + ".cv3");
  return cv3;
}

static IPluginV2Layer* addYoLoLayer(INetworkDefinition *network, std::map<std::string, Weights>& weightMap, std::string lname, std::vector<IConvolutionLayer*> dets) {
    auto creator = getPluginRegistry()->getPluginCreator("YoloLayer_TRT", "1");
    auto anchors = getAnchors(weightMap, lname);
    PluginField plugin_fields[2];
    int netinfo[4] = {kNumClass, kInputW, kInputH, kMaxNumOutputBbox};
    plugin_fields[0].data = netinfo;
    plugin_fields[0].length = 4;
    plugin_fields[0].name = "netinfo";
    plugin_fields[0].type = PluginFieldType::kFLOAT32;

    //load strides from Detect layer
    assert(weightMap.find(lname + ".strides") != weightMap.end() && "Not found `strides`, please check gen_wts.py!!!");
    Weights strides = weightMap[lname + ".strides"];
    auto *p = (const float*)(strides.values);
    std::vector<int> scales(p, p + strides.count);

    std::vector<YoloKernel> kernels;
    for (size_t i = 0; i < anchors.size(); i++) {
        YoloKernel kernel;
        kernel.width = kInputW / scales[i];
        kernel.height = kInputH / scales[i];
        memcpy(kernel.anchors, &anchors[i][0], anchors[i].size() * sizeof(float));
        kernels.push_back(kernel);
    }
    plugin_fields[1].data = &kernels[0];
    plugin_fields[1].length = kernels.size();
    plugin_fields[1].name = "kernels";
    plugin_fields[1].type = PluginFieldType::kFLOAT32;
    PluginFieldCollection plugin_data;
    plugin_data.nbFields = 2;
    plugin_data.fields = plugin_fields;
    IPluginV2 *plugin_obj = creator->createPlugin("yololayer", &plugin_data);
    std::vector<ITensor*> input_tensors;
    for (auto det: dets) {
        input_tensors.push_back(det->getOutput(0));
    }
    auto yolo = network->addPluginV2(&input_tensors[0], input_tensors.size(), *plugin_obj);
    return yolo;
}

ICudaEngine* build_det_engine(unsigned int maxBatchSize, IBuilder* builder, IBuilderConfig* config, DataType dt, float& gd, float& gw, std::string& wts_name) {
  INetworkDefinition* network = builder->createNetworkV2(0U);

  // Dims32 dimsInput;
  // dimsInput.d[0] = maxBatchSize;
  // dimsInput.d[1] = 3;
  // dimsInput.d[2] = kInputH;
  // dimsInput.d[3] = kInputW;
  // dimsInput.nbDims = 4;
  ITensor* data = network->addInput(kInputTensorName, dt, Dims3{ 3, kInputH, kInputW });
  assert(data);

  std::map<std::string, Weights> weightMap = loadWeights(wts_name);
  std::cout << "ok loaded\n";

  //Backbone
  auto model0 = StemBlock(network, weightMap, *data, "model.0");
  auto model1 = ShuffleV2Block(network, weightMap, *model0->getOutput(0), 32, 128, 2, "model.1");  

  auto model2_0 = ShuffleV2Block(network, weightMap, *model1->getOutput(0), 128, 128, 1, "model.2.0" );
  auto model2_1 = ShuffleV2Block(network, weightMap, *model2_0->getOutput(0), 128, 128, 1, "model.2.1" );
  auto model2_2 = ShuffleV2Block(network, weightMap, *model2_1->getOutput(0), 128, 128, 1, "model.2.2" );
  
  auto model3 = ShuffleV2Block(network, weightMap, *model2_2->getOutput(0), 128, 256, 2, "model.3");  
  
  auto model4_0 = ShuffleV2Block(network, weightMap, *model3->getOutput(0), 256, 256, 1, "model.4.0" );
  auto model4_1 = ShuffleV2Block(network, weightMap, *model4_0->getOutput(0), 256, 256, 1, "model.4.1" );
  auto model4_2 = ShuffleV2Block(network, weightMap, *model4_1->getOutput(0), 256, 256, 1, "model.4.2" );
  auto model4_3 = ShuffleV2Block(network, weightMap, *model4_2->getOutput(0), 256, 256, 1, "model.4.3" );
  auto model4_4 = ShuffleV2Block(network, weightMap, *model4_3->getOutput(0), 256, 256, 1, "model.4.4" );
  auto model4_5 = ShuffleV2Block(network, weightMap, *model4_4->getOutput(0), 256, 256, 1, "model.4.5" );
  auto model4_6 = ShuffleV2Block(network, weightMap, *model4_5->getOutput(0), 256, 256, 1, "model.4.6" );

  auto model5 = ShuffleV2Block(network, weightMap, *model4_6->getOutput(0), 256, 512, 2, "model.5" );

  auto model6_0 = ShuffleV2Block(network, weightMap, *model5->getOutput(0), 512, 512, 1, "model.6.0" );
  auto model6_1 = ShuffleV2Block(network, weightMap, *model6_0->getOutput(0), 512, 512, 1, "model.6.1" );
  auto model6_2 = ShuffleV2Block(network, weightMap, *model6_1->getOutput(0), 512, 512, 1, "model.6.2" );

  auto model7 = convBlock(network, weightMap, *model6_2->getOutput(0), 128, 1, 1, 1, 0, "model.7");
  
  auto model8 =  network->addResize(*model7->getOutput(0));
  assert(model8);
  model8->setResizeMode(ResizeMode::kNEAREST);
  std::vector<float> scales = { 1.0f, 2.0f, 2.0f}; // For RGB channels, use 1.0f for no scaling
  model8->setScales(scales.data(), 3);

  ITensor* inputTensors9[] = { model8->getOutput(0), model4_6->getOutput(0)};

  auto model9 = network->addConcatenation(inputTensors9, 2);

  auto model10 = C3(network, weightMap, *model9->getOutput(0), 384, 128, 1, false, 1, 0.5, "model.10");
  
  auto model11 = convBlock(network, weightMap, *model10->getOutput(0), 128, 1, 1, 1, 0, "model.11");

  auto model12 = network->addResize(*model11->getOutput(0));
  model12->setScales(scales.data(), 3);

  ITensor* inputTensors13[] = {model12->getOutput(0), model2_2->getOutput(0)};
  auto model13 = network->addConcatenation(inputTensors13, 2);

  auto model14 = C3(network, weightMap, *model13->getOutput(0), 256, 128, 1, false, 1, 0.5, "model.14");

  auto model15 = convBlock(network, weightMap, *model14->getOutput(0), 128, 3, 2, 1, 1, "model.15");

  ITensor* inputTensors16[] = {model15->getOutput(0), model11->getOutput(0)};
  auto model16 = network->addConcatenation(inputTensors16, 2);

  auto model17 = C3(network, weightMap, *model16->getOutput(0), 256, 128, 1, false, 1, 0.5, "model.17");

  auto model18 = convBlock(network, weightMap, *model17->getOutput(0), 128, 3, 2, 1, 1, "model.18");

  ITensor* inputTensors19[] = {model18->getOutput(0), model7->getOutput(0)};
  auto model19 = network->addConcatenation(inputTensors19, 2);

  auto model20 = C3(network, weightMap, *model19->getOutput(0), 256, 128, 1, false, 1, 0.5, "model.20");
  
  IConvolutionLayer* model21_0 = network->addConvolution(*model14->getOutput(0), 48, DimsHW(1, 1), weightMap["model.21.m.0.weight"], weightMap["model.21.m.0.bias"]);
  
  IConvolutionLayer* model21_1 = network->addConvolution(*model17->getOutput(0), 48, DimsHW(1, 1), weightMap["model.21.m.1.weight"], weightMap["model.21.m.1.bias"]);
  IConvolutionLayer* model21_2 = network->addConvolution(*model20->getOutput(0), 48, DimsHW(1, 1), weightMap["model.21.m.2.weight"], weightMap["model.21.m.2.bias"]);

  auto yolo = addYoLoLayer(network, weightMap, "model.21", std::vector<IConvolutionLayer*>{model21_0, model21_1, model21_1});
  
  std::cout << "Output dims: ";
  print_dims(*yolo->getOutput(0));

  yolo->getOutput(0)->setName(kOutputTensorName);
  network->markOutput(*yolo->getOutput(0));

  builder->setMaxBatchSize(maxBatchSize);

  config->setMaxWorkspaceSize(16 * (1 << 20)); 

#if defined(USE_FP16)
  config->setFlag(BuilderFlag::);
#endif

  ICudaEngine* engine = builder->buildEngineWithConfig(*network, *config);
  std::cout << "Created engine !!!\n";
  network->destroy();
  for (auto& mem : weightMap) {
      free((void*)(mem.second.values));
  }
  return engine;
}